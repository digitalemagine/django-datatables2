# -*- coding: utf-8 -*-
from .mixins import JSONResponseView, JSONResponseMixin
from django.views.generic.base import TemplateView
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import ImproperlyConfigured


class DatatableMixin(object):
    """ JSON data for datatables
    """
    model = None
    columns = []
    order_columns = []
    max_display_length = 100  # max limit of records returned, do not allow to kill our server by huge sets of data

    json_list_url_name = None  # built with a reverse lazy
    # should I add more stuff here or... make it nicer and extensible with context?
    title = None

    def initialize(*args, **kwargs):
        pass

    def get_order_columns(self):
        """ Return list of columns used for ordering
        """
        return self.order_columns

    def get_columns(self):
        """ Returns the list of columns that are returned in the result set
        """
        if not self.columns:
            # Autodiscover!
            self.columns = self.model._meta.get_all_field_names()
        return self.columns

    def render_column(self, row, column):
        """ Renders a column on a row
        """
        if hasattr(row, 'get_%s_display' % column):
            # It's a choice field
            text = getattr(row, 'get_%s_display' % column)()
        else:
            try:
                text = getattr(row, column)
            except AttributeError:
                obj = row
                for part in column.split('.'):
                    if obj is None:
                        break
                    try:
                        obj = getattr(obj, part)
                    except AttributeError as e:
                        raise ImproperlyConfigured('Error! "{}" is not a field of "{}" ({}). Please remove it from "columns" or define a custom behaviour overriding "render_column"'.format(part, obj, obj.__class__))

                text = obj

        if hasattr(row, 'get_absolute_url'):
            return '<a href="%s">%s</a>' % (row.get_absolute_url(), text)
        else:
            return text

    def ordering(self, qs):
        """ Get parameters from the request and prepare order by clause
        """
        request = self.request
        # Number of columns that are used in sorting
        try:
            i_sorting_cols = int(request.REQUEST.get('iSortingCols', 0))
        except ValueError:
            i_sorting_cols = 0

        order = []
        order_columns = self.get_order_columns()
        for i in range(i_sorting_cols):
            # sorting column
            try:
                i_sort_col = int(request.REQUEST.get('iSortCol_%s' % i))
            except ValueError:
                i_sort_col = 0
            # sorting order
            s_sort_dir = request.REQUEST.get('sSortDir_%s' % i)

            sdir = '-' if s_sort_dir == 'desc' else ''

            try:
                sortcol = order_columns[i_sort_col]
                if isinstance(sortcol, list):
                    for sc in sortcol:
                        order.append('%s%s' % (sdir, sc))
                else:
                    order.append('%s%s' % (sdir, sortcol))
            except IndexError:
                # this is not a sortable column!
                # @TODO - manage this differently?
                """
                Maybe this case should create a not-sortable column
                in the javascript settings automatically.
                Or find another way to sort...
                """
                pass

        if order:
            return qs.order_by(*order)
        return qs

    def paging(self, qs):
        """ Paging
        """
        limit = min(int(self.request.REQUEST.get('iDisplayLength', 10)), self.max_display_length)
        # if pagination is disabled ("bPaginate": false)
        if limit == -1:
            return qs
        start = int(self.request.REQUEST.get('iDisplayStart', 0))
        offset = start + limit
        return qs[start:offset]

    def get_initial_queryset(self):
        if not self.model:
            raise NotImplementedError("Need to provide a model or implement get_initial_queryset!")
        return self.model.objects.all()

    def filter_queryset(self, qs):
        return qs

    def prepare_results(self, qs):
        data = []
        for item in qs:
            data.append([self.render_column(item, column) for column in self.get_columns()])
        return data

    def get_json_list_url(self, kwargs):
        #  you can override this to customize the url
        if not self.json_list_url_name:
            raise ImproperlyConfigured('Missing "json_list_url_name" for {}'.format(self.__class__))
        return reverse_lazy(self.json_list_url_name)

    def get_context_data_json(self, *args, **kwargs):
        request = self.request
        self.initialize(*args, **kwargs)

        qs = self.get_initial_queryset()

        # number of records before filtering
        total_records = qs.count()

        qs = self.filter_queryset(qs)

        # number of records after filtering
        total_display_records = qs.count()

        qs = self.ordering(qs)
        qs = self.paging(qs)

        # prepare output data
        aaData = self.prepare_results(qs)

        ret = {'sEcho': int(request.REQUEST.get('sEcho', 0)),
               'iTotalRecords': total_records,
               'iTotalDisplayRecords': total_display_records,
               'aaData': aaData
        }
        return ret

    def get_context_data(self, *args, **kwargs):

        context = self.get_context_data_json(self, *args, **kwargs)

        context.update({
            # the following is used in the template, not the datatable!
               'columns': self.get_columns(),
               'order_columns': self.get_order_columns(),
               'json_list_url': self.get_json_list_url(kwargs),
               'title': self.title,
               })

        return context


class BaseDatatableView(DatatableMixin, JSONResponseView):

    def get(self, request, *args, **kwargs):

        if 'text/json' in request.META.get('HTTP_ACCEPT') or 'application/json' in request.META.get('HTTP_ACCEPT') or request.GET.get('type')=='json':
            return JSONResponseMixin.get(self, request, *args, **kwargs)
        else:
            ### Allow to return a template with the context, rather than just JSON.
            # this contains
            # There are probably better ways to do it... probably better NOT to use the full context.
            # Just testing now...
            return TemplateView.get(self, request, *args, **kwargs)


