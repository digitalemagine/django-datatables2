from setuptools import setup, find_packages
import os

version = '2.0'

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.md')).read()

setup(name='django-datatables2',
      version=version,
      description='Django datatables view 2',
      long_description=README,
      url='https://bitbucket.org/digitalemagine/django-datatables2',
      classifiers=[
          'Environment :: Web Environment',
          'Framework :: Django',
          'License :: OSI Approved :: BSD License',
          'Operating System :: OS Independent',
          'Programming Language :: Python',
      ],
      keywords='django datatables view',
      author='Stefano Crosta, Maciej Wisniowski',
      author_email='stefano@digitalemagine.com, maciej.wisniowski@natcam.pl',
      license='BSD',
      packages=find_packages(exclude=['ez_setup']),
      include_package_data=True,
      zip_safe=False,
      dependency_links=[],
      install_requires=[
          'setuptools',
      ],
     )
